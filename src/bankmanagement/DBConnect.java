/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankmanagement;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author DELL
 */
public class DBConnect {
    private Connection DBConnect;
    public Connection connect()
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Connection Successful");
        }
        catch(ClassNotFoundException e)
        {
            System.out.println("connection fail"+e);
        }
        String url = "jdbc:mysql://localhost:3306/Banking";
        try
        {
            DBConnect = (Connection) DriverManager.getConnection(url, "root", "");
            System.out.println("database Connected");
        }
        catch(SQLException se)
        {
            System.out.println("No database"+se);
        }
        return DBConnect;
    }
}
